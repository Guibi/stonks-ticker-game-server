--@BLOCK Create database
DROP DATABASE stonks_ticker;
CREATE DATABASE stonks_ticker;
USE stonks_ticker;

CREATE TABLE Users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255),
    email VARCHAR(255),
    passwd binary(32)
);

CREATE TABLE Games (
    id INT PRIMARY KEY AUTO_INCREMENT,
    code VARCHAR(6),
    value_a INT,
    value_b INT,
    value_c INT,
    value_d INT,
    value_e INT,
    value_f INT,
    value_g INT,
    value_h INT
);

CREATE TABLE Players (
    id INT PRIMARY KEY AUTO_INCREMENT,
    user_id INT NOT NULL,
    game_id INT NOT NULL,
    amount_a INT,
    amount_b INT,
    amount_c INT,
    amount_d INT,
    amount_e INT,
    amount_f INT,
    amount_g INT,
    amount_h INT,
    amount_money INT,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    FOREIGN KEY (game_id) REFERENCES Games(id)
);