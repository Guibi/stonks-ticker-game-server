export enum TypeEvent
{
    Handshake,
    SignUp,
    Login,
    CreateGame,
    JoinGame,
    QuitGame,
    PlayerData,
    GameData,
    Ready,
    InvalidRequest
}

export enum ResultEvent
{
    Fail,
    Success
}