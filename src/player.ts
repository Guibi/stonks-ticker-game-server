export class Player
{
    constructor(id: number, user_id: number, game_id: number, money: number, amount_a: number, amount_b: number, amount_c: number, amount_d: number, amount_e: number, amount_f: number, amount_g: number, amount_h: number)
    {
        this.id = id;
        this.game_id = game_id;
        this.user_id = user_id;
        this.amount_money = money;

        this.amount_a = amount_a;
        this.amount_b = amount_b;
        this.amount_c = amount_c;
        this.amount_d = amount_d;
        this.amount_e = amount_e;
        this.amount_f = amount_f;
        this.amount_g = amount_g;
        this.amount_h = amount_h;
    }
    
    readonly id: number;

    readonly user_id: number;
    readonly game_id: number;

    amount_a: number;
    amount_b: number;
    amount_c: number;
    amount_d: number;
    amount_e: number;
    amount_f: number;
    amount_g: number;
    amount_h: number;

    amount_money: number;
}