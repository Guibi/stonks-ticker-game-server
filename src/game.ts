import { randomInt } from 'crypto';
import { Database } from './database';
import { Player } from './player';


export class Game
{
    constructor(database: Database, id: number, code: string, players: Player[], value_a: number, value_b: number, value_c: number, value_d: number, value_e: number, value_f: number, value_g: number, value_h: number)
    {
        this.database = database;
        this.id = id;
        this.code = code;
        this.players = players;
        
        this.value_a = value_a;
        this.value_b = value_b;
        this.value_c = value_c;
        this.value_d = value_d;
        this.value_e = value_e;
        this.value_f = value_f;
        this.value_g = value_g;
        this.value_h = value_h;
    }

    async newTurn()
    {
        let stock = <Stock><unknown> Stock[randomInt(8)];
        let action = <Action><unknown> Action[randomInt(3)];
        let value = <Value><unknown> Value[randomInt(3)];

        if (action == Action.Div)
            await this.dividende(stock, value);

        else
        {
            if (action == Action.Down)
                value *= -1;
            
            switch (stock)
            {
                case Stock.A:
                    this.value_a += value;
                    break;

                case Stock.B:
                    this.value_b += value;
                    break;

                case Stock.C:
                    this.value_c += value;
                    break;

                case Stock.D:
                    this.value_d += value;
                    break;

                case Stock.E:
                    this.value_e += value;
                    break;

                case Stock.F:
                    this.value_f += value;
                    break;

                case Stock.G:
                    this.value_g += value;
                    break;

                case Stock.H:
                    this.value_h += value;
                    break;
            }
        }

        this.database.updateGame(this);
    }

    private async dividende(stock: Stock, value: Value)
    {
        let players = await this.getPlayer(this.id);

        let player: any;
        for (player in players)
        {
            switch (stock)
            {
                case Stock.A:
                    var amount = player.amount_a;
                    break;

                case Stock.B:
                    var amount = player.amount_b;
                    break;

                case Stock.C:
                    var amount = player.amount_c;
                    break;

                case Stock.D:
                    var amount = player.amount_d;
                    break;

                case Stock.E:
                    var amount = player.amount_e;
                    break;

                case Stock.F:
                    var amount = player.amount_f;
                    break;

                case Stock.G:
                    var amount = player.amount_g;
                    break;

                case Stock.H:
                    var amount = player.amount_h;
                    break;
            }

            player.amount_money += amount * (1 + (value / 100));
            this.database.updatePlayer(player);
        }
    }

    getPlayers()
    {
        return this.players;
    }

    getPlayer(userID: number)
    {
        for (let i = 0; i < this.players.length; i++)
            if (this.players[i].user_id == userID)
                return this.players[i];
    }

    getA()
    {
        return this.value_a;
    }

    getB()
    {
        return this.value_b;
    }

    getC()
    {
        return this.value_c;
    }

    getD()
    {
        return this.value_d;
    }

    getE()
    {
        return this.value_e;
    }

    getF()
    {
        return this.value_f;
    }

    getG()
    {
        return this.value_g;
    }

    getH()
    {
        return this.value_h;
    }
    

    private database: Database;
    
    readonly id: number;
    readonly code: string;
    private players: Player[];

    private value_a: number;
    private value_b: number;
    private value_c: number;
    private value_d: number;
    private value_e: number;
    private value_f: number;
    private value_g: number;
    private value_h: number;
}

// Enums
enum Stock
{
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H
}

enum Action
{
    Up,
    Down,
    Div
}

enum Value
{
    five = 5,
    ten = 10,
    twenty = 20
}