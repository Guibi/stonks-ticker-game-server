import debug from 'debug';

import { Database } from './database';
import { GameServer } from './server';


debug.enable("database,database:*,server,server:*,socket.io:server");

let server: GameServer;
let database = new Database();
database.connect().then(async () =>
{
    server = new GameServer(database);
    server.start();
});
