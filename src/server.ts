import { Server, Socket } from 'socket.io';
import { Database } from './database';
import { ResultEvent, TypeEvent } from './socketEnums';

import createDebug from 'debug';
const debug = createDebug("server");


export class GameServer
{
    constructor(database: Database)
    {
        this.database = database;
    }
    
    start()
    {
        this.srv = new Server(27030, { });

        this.srv.on("connection", (socket: Socket) => {
            debug.extend("connection")("New connection. Waiting for the handshake...");
            socket.once(TypeEvent.Handshake.toString(), (version: number) => {
                if (version == this.version)
                {
                    socket.send(TypeEvent.Handshake, ResultEvent.Success);
                    debug.extend("event").extend("handshake")("Success.");
                    this.addListeners(socket);
                }

                else
                {
                    socket.disconnect(true);
                    debug.extend("event").extend("handshake")("Fail: versions didn't match.");
                }
            });
        });
    }

    stop()
    {
        this.srv.close();
        for (const clientId in this.srv.allSockets())
            this.srv.sockets.sockets.get(clientId)?.disconnect(true);

        this.database.disconnect();
    }

    private addListeners(socket: Socket)
    {
        const eventDebug = debug.extend("event");

        const debugSignUp = eventDebug.extend("sign-up");
        socket.on(TypeEvent.SignUp.toString(), async (data) => {
            // Get data
            let username: string = data.username;
            let email: string = data.email;
            let password: string = data.password;

            // Verify data
            if (username == undefined || email == undefined || password == undefined)
            {
                debugSignUp("Fail: invalid data '%o'.", data);
                socket.send(TypeEvent.Login, ResultEvent.Fail);
            }

            // Username is invalid
            else if (!(new RegExp("^[\\w-]+$").test(username)))
            {
                debugSignUp("Fail: username '%s' isn't valid.", username);
                socket.send(TypeEvent.SignUp, ResultEvent.Fail);
            }
                
            // Email is invalid
            else if (!(new RegExp("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$").test(email)))
            {
                debugSignUp("Fail: email '%s' isn't valid.", email);
                socket.send(TypeEvent.SignUp, ResultEvent.Fail);
            }

            // Password is invalid
            else if (password.length < 6)
            {
                debugSignUp("Fail: password isn't valid.");
                socket.send(TypeEvent.SignUp, ResultEvent.Fail);
            }

            // Creating new user
            else
            {
                let user = await this.database.addUser(username, email, password);

                if (user != undefined)
                {
                    debugSignUp("Success: new user '%s' is now logged in.", user.username);
                    socket.data.user = user;
                    socket.send(TypeEvent.SignUp, ResultEvent.Success);
                }

                else
                {
                    debugSignUp("Fail: Couldn't add user '%s' with email '%s'.", username, email);
                    socket.send(TypeEvent.SignUp, ResultEvent.Fail);
                }
            }
        });

        const debugLogin = eventDebug.extend("login");
        socket.on(TypeEvent.Login.toString(), async (data) => {
            // Get data
            let username: string = data.username;
            let email: string = data.email;
            let password: string = data.password;

            // Verify data
            if (username == undefined || email == undefined || password == undefined)
            {
                debugLogin("Fail: invalid data '%o'.", data);
                socket.send(TypeEvent.Login, ResultEvent.Fail);
            }

            // Check if username is invalid
            else if (!(new RegExp("^[\\w-]+$").test(username)))
            {
                debugLogin("Fail: username '%s' isn't valid.", username);
                socket.send(TypeEvent.Login, ResultEvent.Fail);
            }

            else
            {
                let user = await this.database.getUser(username);
    
                // Login successful
                if (user != undefined && (await this.database.verifyUserPassword(username, password)))
                {
                    debugLogin("Success: user '%s' is now logged in.", username);
                    socket.data.user = user;
                    socket.send(TypeEvent.Login, ResultEvent.Success);
                }

                // User doesn't exist or password is wrong
                else
                {
                    debugLogin("Fail: invalid login info.");
                    socket.send(TypeEvent.Login, ResultEvent.Fail);
                }
            }
        });
        
        const debugCreateGame = eventDebug.extend("create-game");
        socket.on(TypeEvent.CreateGame.toString(), async () => {
            if (socket.data.game != undefined)
            {
                debugCreateGame("Fail: user '%s' is already in a game.", socket.data.user.username);
                socket.send(TypeEvent.CreateGame, ResultEvent.Fail);
            }

            else
            {
                debugCreateGame("Success: user '%s' created a new game.", socket.data.user.username);
                let game = await this.database.addGame();
                this.database.addPlayer(socket.data.user, game, 5000);
                socket.join(game.code);
                socket.data.game = game;

                socket.send(TypeEvent.CreateGame, ResultEvent.Success);
                socket.send(TypeEvent.JoinGame, ResultEvent.Success);
                debugCreateGame("Success: user '%s' joined game '%s'.", socket.data.user.username, game.code);
            }
        });

        const debugJoinGame = eventDebug.extend("join-game");
        socket.on(TypeEvent.JoinGame.toString(), async (code: string) => {
            // Code is valid
            if (code != undefined && new RegExp("^\\w{6}$").test(code))
            {
                let game = await this.database.getGame(code);

                if (game != undefined)
                {
                    socket.join(game.code);
                    socket.data.game = game;
                    socket.send(TypeEvent.JoinGame, ResultEvent.Success);
                    
                    if (socket.data.game.getPlayer(socket.data.user.id) == undefined)
                    {
                        debugJoinGame("Success: user '%s' is joining game '%s' for the first time with %d$.", socket.data.user.username, socket.data.game.code, 5000);
                        await this.database.addPlayer(socket.data.user, game, 5000);
                    }

                    else
                        debugJoinGame("Success: user '%s' joined game '%s'.", socket.data.user.username, socket.data.game.code);
                }

                else
                {
                    debugJoinGame("Fail: user '%s' submited invalid code '%s'.", socket.data.user.username, code);
                    socket.send(TypeEvent.JoinGame, ResultEvent.Fail);
                }
            }

            else
            {
                debugJoinGame("Fail: user '%s' submited invalid code '%s'.", socket.data.user.username, code);
                socket.send(TypeEvent.JoinGame, ResultEvent.Fail);
            }

        });

        const debugQuitGame = eventDebug.extend("quit-game");
        socket.on(TypeEvent.QuitGame.toString(), () => {
            if (socket.data.game != undefined)
            {
                debugQuitGame("Success: '%s' quit the game '%s'.", socket.data.user.username, socket.data.game.code);
                socket.leave(socket.data.game.code);
                socket.data.game = undefined;
                socket.send(TypeEvent.QuitGame, ResultEvent.Success);
            }

            else
            {
                debugQuitGame("Fail: user '%s' is not in a game.", socket.data.user.username);
                socket.send(TypeEvent.QuitGame, ResultEvent.Fail);
            }
        });

        const debugPlayerData = eventDebug.extend("player-data");
        socket.on(TypeEvent.PlayerData.toString(), () => {
            if (socket.data.game != undefined)
            {
                debugPlayerData("Success: sent game to user '%s'.", socket.data.user.username);

                let player: any;
                for (player in socket.data.game.getPlayers())
                    socket.send(TypeEvent.PlayerData, ResultEvent.Success, player);
            }

            else
            {
                debugPlayerData("Fail: user '%s' is not in a game.", socket.data.user.username);
                socket.send(TypeEvent.PlayerData, ResultEvent.Fail);
            }
        });

        const debugGameData = eventDebug.extend("game-data");
        socket.on(TypeEvent.GameData.toString(), () => {
            if (socket.data.game != undefined)
            {
                debugGameData("Success: sent game to user '%s'.", socket.data.user.username);
                socket.send(TypeEvent.GameData, ResultEvent.Success, socket.data.game);
            }

            else
            {
                debugGameData("Fail: user '%s' is not in a game.", socket.data.user.username);
                socket.send(TypeEvent.GameData, ResultEvent.Fail);
            }
        });

        const debugReady = eventDebug.extend("ready");
        socket.on(TypeEvent.Ready.toString(), () => {
            let sockets = this.srv.sockets.adapter.rooms.get(socket.data.game.code);

            if (socket.data.game != undefined && sockets != undefined)
            {
                debugReady("Success: user '%s' is now ready", socket.data.user.username);
                socket.send(TypeEvent.Ready, ResultEvent.Success);

                let allReady = true;

                for (const clientId in sockets)
                {
                    const clientSocket = this.srv.sockets.sockets.get(clientId);
                    if (clientSocket?.data.ready != true)
                        allReady = false;
                }
                
                if (allReady)
                    socket.data.game.newTurn();
            }

            else
            {
                debugReady("Fail: user '%s' is not in a game.", socket.data.user.username);
                socket.send(TypeEvent.Ready, ResultEvent.Fail);
            }
        });

        debug("Added all listeners to new socket.");
    }
    

    readonly database: Database;
    private srv!: Server;

    private readonly version = 1;
}