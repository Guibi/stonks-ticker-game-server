export class User
{
    constructor(id: number, username: string, email: string)
    {
        this.id = id;
        this.username = username;
        this.email = email;
    }
    
    readonly id: number;

    readonly username: string;
    readonly email: string;
}