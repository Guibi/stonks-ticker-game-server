import { Connection, createConnection, ResultSetHeader, RowDataPacket } from 'mysql2/promise';
import { Md5 } from 'ts-md5';
import { Game } from './game';
import { Player } from './player';
import { User } from './user';

import createDebug from 'debug';
const debug = createDebug("database");


export class Database
{
    async connect()
    {
        this.connection = await createConnection({
            host: "localhost",
            user: "stonks-ticker-server",
            password: ""
        });
    
        await this.connection.connect();
        await this.connection.query("USE stonks_ticker");
        debug("MySQL database connected!");
    }

    disconnect()
    {
        this.connection.end()
            .then(() => debug("MySQL database disconnected!"));
    }

    async addUser(username: string, email: string, password: string)
    {
        const hash = Md5.hashStr(password);

        if (((await this.connection.execute("SELECT id FROM users WHERE username = ? OR email = ?", [username, email]))[0] as RowDataPacket[])[0] == undefined)
        {
            const result = (await this.connection.execute(
                "INSERT INTO users (username, email, passwd) VALUES (?, ?, ?)",
                [username, email, hash])
            )[0] as ResultSetHeader;
            

            debug("Success: added user '%s' with email '%s'.", username, email);
            return new User(result.insertId, username, email);
        }
        
        else
            debug("Fail: user '%s' already exists !", username);
    }

    async getUser(username: string)
    {
        const result = ((await this.connection.execute("SELECT * FROM users WHERE username = ?", [username]))[0] as RowDataPacket[])[0];

        if (result != undefined)
        {
            debug("Success: user '%s' fetched from database.", username);
            return new User(result.id, result.username, result.email);
        }

        else
            debug("Fail: user '%s' does not exist in database.", username);
    }

    async verifyUserPassword(username: string, password: string)
    {
        const hash = Md5.hashStr(password);

        const result = ((await this.connection.execute("SELECT id FROM users WHERE username = ? AND passwd = ?", [username, hash]))[0] as RowDataPacket[])[0];
        
        if (result != undefined)
        {
            debug("Success: password for user '%s' is correct.", username);
            return true;
        }

        else
        {
            debug("Fail: password for user '%s' does not match with database.", username);
            return false;
        }
    }

    async addGame()
    {
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let code = '';

        do
        {
            for (var i = 0; i < 6; i++)
                code += characters.charAt(Math.floor(Math.random() * characters.length));
        } while (((await this.connection.execute("SELECT id FROM games WHERE code = ?", [code]))[0] as RowDataPacket[])[0] != undefined);
        
        const result = (await this.connection.execute(
            "INSERT INTO games " +
            "(code, value_a, value_b, value_c, value_d, value_e, value_f, value_g, value_h)" +
            "VALUES (?, 100, 100, 100, 100, 100, 100, 100, 100)",
            [code])
        )[0] as ResultSetHeader;

        debug("Success: created a new game with code '%s'.", code);
        return new Game(this, result.insertId, code, [], 100, 100, 100, 100, 100, 100, 100, 100);
    }

    async getGame(code: string)
    {
        const result = ((await this.connection.execute("SELECT * FROM games WHERE code = ?", [code]))[0] as RowDataPacket[])[0];

        if (result != undefined)
        {
            let players: Player[] = [];
            const playerList = (await this.connection.execute("SELECT * FROM players WHERE game_id = ?", [result.id]))[0] as RowDataPacket[];
            for (let i = 0; i < playerList.length; i++)
            {
                players.push(new Player(
                    playerList[i].id,
                    playerList[i].user_id,
                    playerList[i].game_id,
                    playerList[i].amount_money,
                    playerList[i].amount_a, playerList[i].amount_b, playerList[i].amount_c, playerList[i].amount_d,
                    playerList[i].amount_e, playerList[i].amount_f, playerList[i].amount_g, playerList[i].amount_h
                ));
            }

            debug("Success: game '%s' fetched from database.", code);
            return new Game(
                this,
                result.id,
                code,
                players,
                result.value_a, result.value_b, result.value_c, result.value_d,
                result.value_e, result.value_f, result.value_g, result.value_h
            );
        }

        else
            debug("Fail: game '%s' does not exist in database.", code);
    }
    
    async updateGame(game: Game)
    {
        debug("%O", this.connection.execute(
            "UPDATE games " +
            "SET value_a = ?, value_b = ?, value_c = ?, value_d = ?, value_e = ?, value_f = ?, value_g = ?, value_h = ? " +
            "WHERE id = ?",
            [game.getA(), game.getB(), game.getC(), game.getD(), game.getE(), game.getF(), game.getG(), game.getH(),
            game.id]
        ));
    }

    async addPlayer(user: User, game: Game, money: number)
    {
        const result = (await this.connection.execute(
            "INSERT INTO players " +
            "(user_id, game_id, amount_a, amount_b, amount_c, amount_d, amount_e, amount_f, amount_g, amount_h, amount_money) " +
            "VALUES (?, ?, 0, 0, 0, 0, 0, 0, 0, 0, ?)",
            [user.id, game.id, money])
        )[0] as ResultSetHeader;

        debug("Success: created a new player for user '%s' for game '%s'.", user.username, game.code);
        return new Player(result.insertId, user.id, game.id, money, 0, 0, 0, 0, 0, 0, 0, 0);
    }

    async updatePlayer(players: Player[])
    {
        let player: any;
        for (player in players)
            this.connection.execute(
                "UPDATE players " +
                "SET amount_a = ?, amount_b = ?, amount_c = ?, amount_d = ?, amount_e = ?, amount_f = ?, amount_g = ?, amount_h = ?, amount_money = ? " +
                "WHERE id = ?",
                [player.amount_a, player.amount_b, player.amount_c, player.amount_d, player.amount_e, player.amount_f, player.amount_g, player.amount_h,
                player.id]
            );

        debug("Success: updated players in database.");
    }


    private connection!: Connection;
}

